import math

from SqlConnector.Services.ActionLogService import ActionLogService
from SqlConnector.Services.ActionService import ActionService
from SqlConnector.Services.ConfigService import ConfigService


class Reservoir:
    def __init__(self):
        self.configService = ConfigService()
        self.actionLogService = ActionLogService()
        self.actionService = ActionService()
        self.waterMass = self.configService.makeConfig('waterMass')
        self.specificHeat = self.configService.makeConfig('specificHeat')
        self.lookTemp = self.configService.makeConfig('lookTemperature')

    def getNewTemp(self):
        waterTempAction = self.actionService.makeAction(1)
        deviationAction = self.actionService.makeAction(4)
        heaterAction = self.actionService.makeAction(3)
        heaterPower = self.actionLogService.lastLogValue(heaterAction)
        lastTemperature = self.actionLogService.lastLogValue(waterTempAction)
        increasedTemp = self.calcIncreaseTemp(heaterPower,waterTempAction)
        decreasedTemp = self.calcDecTemp(waterTempAction)
        newTemp = lastTemperature + (increasedTemp - decreasedTemp)
        self.actionLogService.addLog(waterTempAction, newTemp)
        self.actionLogService.addLog(deviationAction,self.lookTemp.getValue() - newTemp)

    def calcIncreaseTemp(self, heaterPower, waterTempAction):
        lastTemperature = self.actionLogService.lastLogValue(waterTempAction)
        newTemp = (360 * heaterPower / (self.waterMass.getValue() * self.specificHeat.getValue()))
        return newTemp

    def calcDecTemp(self,waterTempAction):
        lastTemperature = self.actionLogService.lastLogValue(waterTempAction)
        areaTemp = self.configService.makeConfig('areaTemperature').getValue()
        areaOfCooling = self.configService.makeConfig('areaOfCooling').getValue()
        heatCapacity = self.configService.makeConfig('heatCapacity').getValue()
        k = 0.6 * areaOfCooling / heatCapacity
        newTemp = areaTemp + (lastTemperature - areaTemp) * math.exp(-k*360)
        return lastTemperature - newTemp