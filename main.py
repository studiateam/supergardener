# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
from time import sleep

from PID.PidController import PID
import SqlConnector
from SqlConnector.Services.ConfigService import ConfigService
from TemperatureController.TemperatureController import TemperatureController
from WaterReservoir.Reservoir import Reservoir


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    Controller = TemperatureController()
    while 1:
        Controller.runOneTime()
        sleep(1)

    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
