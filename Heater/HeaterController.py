from SqlConnector.Services.ActionLogService import ActionLogService
from SqlConnector.Services.ActionService import ActionService
from SqlConnector.Services.ConfigService import ConfigService


class Heater:
    def __init__(self):
        self.configService = ConfigService()
        self.actionService = ActionService()
        self.actionLogService = ActionLogService()
        self.minPower = self.configService.makeConfig('minHeaterPower')
        self.maxPower = self.configService.makeConfig('maxHeaterPower')

    def getHeaterPower(self):
        uPIDAction = self.actionService.makeAction(2)
        heaterValueAction = self.actionService.makeAction(3)
        lastuPIDValue = self.actionLogService.lastLogValue(uPIDAction)


        if 0 <= lastuPIDValue <= 100:
            value = (0.01*lastuPIDValue)*self.maxPower.getValue()
            self.actionLogService.addLog(heaterValueAction, round(value,2))
        if lastuPIDValue < 0:
            self.actionLogService.addLog(heaterValueAction, self.minPower.getValue())
        if lastuPIDValue > 100:
            self.actionLogService.addLog(heaterValueAction, self.maxPower.getValue())

