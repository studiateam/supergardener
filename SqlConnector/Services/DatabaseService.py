import mysql.connector


class DatabaseService:

    def __init__(self):
        self.mydb = mysql.connector.connect(
            host ='192.168.1.9',
            # host='sql62.lh.pl',
            # user='serwer96419_reservoir',
            user ='root',
            password='',
            # password='reservoir',
            database='serwer96419_reservoir'
        )

    def executeInsertQuery(self, sql, values):
        cursor = self.mydb.cursor()
        cursor.execute(sql, values)
        self.mydb.commit()
        return cursor.lastrowid

    def executeSelectQuery(self, sql, values):
        cursor = self.mydb.cursor()
        cursor.execute(sql, values)
        return cursor.fetchall()


