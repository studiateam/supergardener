from datetime import datetime
from SqlConnector.Services.DatabaseService import DatabaseService

class ActionLogService:
    def __init__(self):
        self.dB = DatabaseService()


    # Add new log, by given object action
    # Params Action -> Action (object)
    def addLog(self, Action, value):
        sql = "INSERT INTO action_log (action_id_id,action_exec_time,action_new_value) VALUES (%s, %s, %s)"
        now = datetime.now()
        values = (Action.getId(), now, str(value))
        self.dB.executeInsertQuery(sql, values)

    def lastLogValue(self,Action):
        sql = "SELECT action_new_value FROM action_log WHERE action_id_id = %s ORDER BY id DESC LIMIT 1"
        values = (Action.getId(),)
        result = self.dB.executeSelectQuery(sql, values)
        if result != None:
            return result[0][0]
        return None

    def getDeltaOfTwoLogsValue(self,Action):
        sql = "SELECT action_new_value FROM action_log WHERE action_id_id = %s ORDER BY id DESC LIMIT 2"
        values = (Action.getId(),)
        result = self.dB.executeSelectQuery(sql, values)
        if result != None:
            return result[0][0] - result[1][0]
        return None

    def getSumLogValue(self,Action):
        sql = "SELECT SUM(action_new_value) FROM action_log WHERE action_id_id = %s"
        values = (Action.getId(),)
        result = self.dB.executeSelectQuery(sql, values)
        if result != None:
            return result[0][0]
        return None