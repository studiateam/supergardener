from SqlConnector.Entity.Config import Config
from SqlConnector.Services.DatabaseService import DatabaseService

class ConfigService:
    def __init__(self):
        self.dB = DatabaseService()

    # def makeNewConfig(self, configName, value):
    #     sql = "INSERT INTO actions (name,startResponseCode,endResponseCode) VALUES (%s, %s, %s)"
    #     values = (str(name), startResponseCode, endResponseCode)

    # Return Config object it found in database
    # returns None | Config
    def makeConfig(self, configName):
        sql = "SELECT id,config_name, value,string_value, date_value FROM config WHERE config_name = %s"
        values = (configName,)
        config = self.dB.executeSelectQuery(sql, values)
        if config != None:
            return Config(config[0][0],configName, config[0][2], config[0][3], config[0][4])
        return None