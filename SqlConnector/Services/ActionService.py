
from SqlConnector.Entity.Action import Action
from SqlConnector.Services.DatabaseService import DatabaseService


class ActionService:

    def __init__(self):
        self.dB = DatabaseService()

    # Create new Action
    # returns Action
    def makeNewAction(self, name, startResponseCode, endResponseCode):
        sql = "INSERT INTO actions (name,start_response_code,end_response_code) VALUES (%s, %s, %s)"
        values = (str(name),startResponseCode,endResponseCode)
        actionId = self.dB.executeInsertQuery(sql,values)
        return Action(actionId, name, startResponseCode, endResponseCode)

    # Return Action object it found in database
    # returns None | Action
    def makeAction(self,actionId):
        sql = "SELECT name, start_response_code, end_response_code FROM actions WHERE id = %s"
        values = (actionId,)
        action = self.dB.executeSelectQuery(sql,values)
        if action != None:
            return Action(actionId, action[0][0], action[0][1], action[0][2])
        return None

