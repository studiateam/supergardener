class Action:

    def __init__(self, id, name, startResponseCode, endResponseCode):
        self.id = id
        self.name = name
        self.startResponseCode = startResponseCode
        self.endResponseCode  = endResponseCode

    def getId(self):
        return self.id

    def getName(self):
        return self.name

    def getStartResponseCode(self):
        return self.startResponseCode

    def endResponseCode(self):
        return self.endResponseCode

    def setName(self, name):
        self.name = name

    def setStartResponseCode(self, startResponseCode):
        self.startResponseCode = startResponseCode

    def setEndResponseCode(self, endResponseCode):
        self.endResponseCode = endResponseCode
