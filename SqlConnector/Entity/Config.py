class Config:

    def __init__(self, id, configName, value, stringValue,dateValue):
        self.id = id
        self.configName = configName
        self.value = value
        self.stringValue = stringValue
        self.dateValue = dateValue

    def getConfigName(self):
        return str(self.configName)

    def getId(self):
        return int(self.id)

    def getValue(self):
        if None != self.value:
            return self.value

        elif None != self.stringValue:
            return str(self.stringValue)

        elif None != self.dateValue:
            return self.dateValue