class Action:
    def __init__(self, id, action, dateTime):
        self.id = id
        self.actionId = action.getId
        self.date = dateTime

    def getId(self):
        return self.id

    def getActionId(self):
        return self.actionId

    def getDate(self):
        return self.date
