from SqlConnector.Services.ActionLogService import ActionLogService
from SqlConnector.Services.ActionService import ActionService
from SqlConnector.Services.ConfigService import ConfigService


class PID:
    def __init__(self, ):
        self.tI = 0.85
        self.kP = 0.015
        self.tP = 0.1
        self.tD = 0.25
        self.configService = ConfigService()
        self.actionLogService = ActionLogService()
        self.actionService = ActionService()


    def getPidValue(self):
        waterTempAction = self.actionService.makeAction(4)
        lastValue = self.actionLogService.lastLogValue(waterTempAction)
        deltaValue = self.actionLogService.getDeltaOfTwoLogsValue(waterTempAction)
        sumValue = self.actionLogService.getSumLogValue(waterTempAction)

        uPID = (self.kP * lastValue) + ((self.tP / self.tI) * sumValue) + ((self.tD / self.tP) * deltaValue)

        logPIDAction = self.actionService.makeAction(2)
        self.actionLogService.addLog(logPIDAction,uPID)


        return uPID