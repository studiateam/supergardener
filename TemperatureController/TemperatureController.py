from Heater.HeaterController import Heater
from PID.PidController import PID
from WaterReservoir.Reservoir import Reservoir


class TemperatureController:
    def __init__(self):
        self.PIDController = PID()
        self.HeaterController = Heater()
        self.WaterReservoirController = Reservoir()

    def runOneTime(self):
        self.PIDController.getPidValue()
        self.HeaterController.getHeaterPower()
        self.WaterReservoirController.getNewTemp()